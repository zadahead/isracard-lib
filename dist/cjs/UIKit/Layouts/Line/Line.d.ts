import React from "react";
export interface ILineProps {
    children: React.ReactNode;
    className?: string;
}
export declare const Line: React.FC<ILineProps>;
export declare const Between: React.FC<ILineProps>;
