import React from 'react';
declare const _default: {
    title: string;
    component: React.FC<import("./MUIButton").IMUIButtonProps>;
};
export default _default;
export declare const Primary: () => JSX.Element;
